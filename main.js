// Текущее колличество кассет
let cassettesCount = 0

// Рассчет возможности выдачи суммы возвращает массив с купюрами
function ATM(banknotes, amount) {
    banknotes.sort(function (a, b) {
        return a - b;
    });

    let F = new Array(amount+1).fill(0)
    F[0] = 1
    let prev = new Array(amount+1).fill(0)
    for (let i = 0;i < banknotes.length;i++){
        for (let k = amount;k>banknotes[i]-1; k--){
            if (F[k - banknotes[i] ]=== 1 && F[k] === 0){
                F[k] = 1
                prev[k] = banknotes[i]
            }
        }
    }

    if (F[amount]){
        let ans = []
        let k = amount
        while(k > 0){
            ans.push(prev[k])
            k-= prev[k]
        }
        return ans
    }
    else {
        return []
    }

}

// Преобразование массива купюр в объект {номинал:количество}
function getObjectCosts(banknotes){
    let res = {}
    console.log(banknotes)
    for (let banknote of banknotes){
        if (banknote in res){
            res[banknote] += 1
        }
        else {
            res[banknote] = 1
        }
    }
    return res
}

// Динамическое добавление кассет
function getCassettes(){
    cassettesCount = document.getElementById('cassettes-count').value
    let divCassettes = document.getElementById('cassettes')
    if (cassettesCount >=1 && cassettesCount <=8){
        divCassettes.innerHTML = ''
        for (let i = 1;i<=cassettesCount;i++){
            divCassettes.innerHTML += `<div>
                                        Кассета №${i} Номинал:<input id="nominal${i}"/>
                                        Колличество купюр:<input id="banknote-count${i}"/>
                                        Неисправна:<input id="is-work${i}" type="checkbox"/>
                                    </div>`;
        }
    }
    else{
        alert('Введите значение от 1 до 8')
    }
}

// Получение массива банкнот с разбиением на кассеты
function getCassettesFromBanknotes(cassettes, banknotes) {
    let cassetteResults = new Array(Number(cassettesCount)).fill(0);

    for (let i = 0;i<cassettes.length;i++){
        let nominal = cassettes[i].nominal
        let count = cassettes[i].count
        if (nominal in banknotes){
            if (banknotes[nominal] > 0){
                if (banknotes[nominal] <= count){
                    cassetteResults[i] += banknotes[nominal]
                    banknotes[nominal] = 0
                }
                else{
                    cassetteResults[i] += count
                    banknotes[nominal] -= count
                }
            }
        }
    }
    return cassetteResults

}

// Добавление ответа в html
function addAnswerDiv(cassettes, time) {
    let answer = document.getElementById('answer')
    let answerCassettes = document.getElementById('answer-cassettes')
    answerCassettes.innerHTML  = ''
    answer.innerHTML = ''
    if (cassettes.length === 0){
        answer.innerHTML = 'Выдать нельзя'
    }
    else{
        answer.innerHTML = 'Можно выдать'
        answerCassettes = document.getElementById('answer-cassettes')
        answerCassettes.innerHTML = 'Количество использованных купюр:'
        for (let i = 0;i<cassettes.length;i++){
            answerCassettes.innerHTML += `
                                         <div>
                                             Кассета №${i+1}:${cassettes[i]}
                                         </div>
                                         `
        }
        answerCassettes.innerHTML += `
                                         <div>
                                             Количество затраченного времени:${time}мс
                                         </div>
                                         `
    }
}

// Получение ответа и разбиение по кассетам
function getBanknotes() {
    if (cassettesCount > 0){
        let banknotes = [];
        let cassettes = [];
        let amount = Number(document.getElementById('amount').value)

        // Сбор всех банкнот из всех кассет в один массив
        for (let i = 1;i<=cassettesCount;i++){
            let isWorked = document.getElementById(`is-work${i}`)
            if (!isWorked.checked){
                let nominal = Number(document.getElementById(`nominal${i}`).value)
                let banknoteCount = Number(document.getElementById(`banknote-count${i}`).value)
                let currentBanknotes = new Array(banknoteCount).fill(nominal)
                banknotes = banknotes.concat(currentBanknotes)
                cassettes.push({nominal: nominal, count: banknoteCount})
            }
            else{
                cassettes.push({nominal: 1, count: 0})
            }
        }

        // Получение массива c результирующими банкнотами и замер времени
        let start = performance.now()
        let resBanknotes = ATM(banknotes, amount)
        let end = performance.now()

        if (resBanknotes.length === 0){
            // Случай когда выдача невозможна
            addAnswerDiv([], 0)
        }
        else{
            let time = end - start
            // Создание объекта из массива купюр
            resBanknotes = getObjectCosts(resBanknotes)
            // Получение массива купюр с разбиением на кассеты
            let cassetteResults = getCassettesFromBanknotes(cassettes, resBanknotes)
            // динамическое добавление div с ответом
            addAnswerDiv(cassetteResults, time)
        }
    }
    else{
        alert('Создайте кассеты')
    }
}

